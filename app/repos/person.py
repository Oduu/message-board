#this file contains all of the database person table related functions
from repos.conn import DB

class PersonRepo:
	def exists(self, name):
		DB.cursor.execute(""" SELECT count(*) 'count' from person where username = %s """ \
			(name,))
		return DB.cursor.fetchone()['count'] > 0

	def create(self, name, password, email):
		DB.cursor.execute(""" INSERT into person(username, password, email) values(%s, %s, %s)""" \
						,(name, password ,email))
		DB.connection.commit();

	def get_by_username(self, username):
		DB.cursor.execute(""" SELECT * FROM person WHERE username = %s """ \
		,(username,))
		return DB.cursor.fetchone()


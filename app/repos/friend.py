#this file contains all of the database friend table related functions
from repos.conn import DB

class FriendRepo:
	def get_by_person(self, person_id):
		DB.cursor.execute(""" SELECT * FROM friend WHERE friend_id = %s OR person_id = %s """ \
			,(person_id, person_id))
		return DB.cursor.fetchall();
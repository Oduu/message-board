#this file contains all of the database comments table related functions
from repos.conn import DB

class CommentsRepo:
	def get_by_id(self, post_id):
		DB.cursor.execute(""" SELECT C.*, P.username FROM comments as C INNER JOIN person as P ON P.person_id = C.person_id WHERE C.post_id = %s """ \
		,(post_id,))
		return DB.cursor.fetchall()
	def create(self, body, post_id, person_id):
		DB.cursor.execute(""" INSERT INTO comments(body, post_id, person_id) values(%s, %s, %s)""" \
						,(body, post_id, person_id))
		DB.connection.commit();
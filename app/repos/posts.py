#this file contains all of the database person table related functions
from repos.conn import DB

class PostsRepo:
	def create(self, person_id, board_id, title, body):
		DB.cursor.execute(""" INSERT into posts(person_id, board_id, title, body) values(%s, %s, %s, %s)""" \
						,(person_id, board_id, title, body))
		DB.connection.commit();

	def get_by_person(self, person_id):
		DB.cursor.execute(""" SELECT * FROM posts WHERE person_id = %s """ \
		,(person_id,))
		return DB.cursor.fetchall()

	def get_by_id(self, post_id):
		DB.cursor.execute(""" SELECT * FROM posts WHERE post_id = %s """ \
		,(post_id,))
		return DB.cursor.fetchone()

	def update_by_id(self, post_id, title, body, person_id):
		DB.cursor.execute(""" UPDATE posts SET title = %s, body = %s WHERE post_id = %s AND person_id = %s """ \
		,(title, body, post_id, person_id))
		DB.connection.commit();

	def get_by_board(self, board_id):
		DB.cursor.execute(""" SELECT * FROM posts WHERE board_id = %s """ \
		,(board_id,))
		return DB.cursor.fetchall()

	def get_last_five_person(self, person_id):
		DB.cursor.execute(""" SELECT * FROM posts WHERE person_id = %s ORDER BY created DESC LIMIT 5 """ \
			,(person_id))
		return DB.cursor.fetchall();

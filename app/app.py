from flask import Flask, render_template, request, redirect, url_for, abort, session, g
import bcrypt
import os
import datetime
from repos.person import PersonRepo
from repos.posts import PostsRepo
from repos.comments import CommentsRepo
from repos.boards import BoardsRepo
from repos.friend import FriendRepo

app = Flask(__name__)
app.secret_key = b'\xebkY\x8e\xb7\xf5\xa3\xbe\xfa(|M\x186\x12v\xba\x04aGuEX\xd0'

@app.route('/signup', methods=["GET","POST"])
def insertUser():
	
	if request.method == 'GET':
		return render_template("signup.html")
	#retrieves details from sign up form and hashes password
	name = str(request.form["user"])
	textpassword = str(request.form["password"])
	encode_psw = textpassword.encode('utf-8')
	password = bcrypt.hashpw(encode_psw, bcrypt.gensalt()).decode('utf-8')
	email = str(request.form["email"])

	#readies class PersonRepo from person.py
	pr = PersonRepo()
	exists = pr.exists(name)

	#basic validation
	if textpassword == "" or name == "" or email == "":
		return render_template("signup.html", error='Please fill out all details!')
	elif exists:
		return render_template("signup.html", error='Username already exists!')
	else:	
		pr.create(name, password, email)

		return redirect(url_for("login"))

@app.route('/login', methods=["GET", "POST"])
def login():

	if request.method == 'POST':
		session.pop('user', None) #clears session before login
		name = str(request.form["user"])
		entered_psw = str(request.form["password"]).encode('utf-8')
		pr = PersonRepo()
		person = pr.get_by_username(name)

		#stops error is username is not in person table
		if person is None:
			return render_template("login.html", error='Login details incorrect')
		#compared hashed password against entered password
		elif bcrypt.checkpw(entered_psw, person['password'].encode('utf-8')):
			session['user'] = name
			session['person_id'] = person['person_id']
			return redirect(url_for('home'))
		else:
			return render_template("login.html", error='Login details incorrect')

	return render_template("login.html")

@app.route("/", methods=['GET', 'POST'])
def home():
	auth()
	posts = PostsRepo().get_last_five_person(g.id)
	return render_template("home.html", posts=posts)

@app.route("/post/<post_id>" , methods=["GET", "POST"])
def post(post_id=None):
	auth()

	if request.method == 'GET':
		if post_id is not None:
			post = PostsRepo().get_by_id(post_id)
			comments = CommentsRepo().get_by_id(post_id)
			return render_template("post.html", post=post, comments=comments)

	if request.method == 'POST':
		comment_body = request.form['new-comment']
		new_comment = CommentsRepo().create(comment_body, post_id, g.id)
		return redirect(url_for('post', post_id=post_id))

	return render_template("home.html", error='Invalid Post')


@app.route("/post/edit/<post_id>", methods=["GET", "POST"])
def edit_post(post_id=None):
	auth()
	if request.method == 'GET':
		post = PostsRepo().get_by_id(post_id)
		return render_template("editpost.html", post=post)

	if request.method == 'POST':
		title = request.form['post-title']
		body = request.form['post-body']
		update_post = PostsRepo().update_by_id(post_id, title, body, g.id)
		return redirect(url_for('post', post_id=post_id))

	return render_template("home.html", error='Invalid post')


@app.route("/newpost", methods=["GET", "POST"])
def new_post():
	auth() 
	boardList = [x["board_id"] for x in BoardsRepo().get_all()]
	if request.method == 'POST':
		title = request.form['post-title']
		body = request.form['post-body']
		board_id = request.form['board-select']
		new_post = PostsRepo().create(g.id, board_id, title, body)
		return redirect(url_for('home'))

	return render_template("newpost.html", boardList=boardList)


@app.route("/myposts")
def my_posts():
	auth()
	posts = PostsRepo().get_by_person(g.id)
	return render_template("myposts.html", posts=posts)

@app.route("/boards")
def boards():
	auth()
	br = BoardsRepo()
	boards = BoardsRepo().get_all()
	boardList = [x["board_id"] for x in BoardsRepo().get_all()]
	print(boards)
	print(boardList)
	return render_template("boards.html", boards=boards, boardList=boardList)

@app.route("/boards/<board_id>")
def board(board_id=None):
	auth()
	posts = PostsRepo().get_by_board(board_id)
	board = BoardsRepo().get_by_id(board_id)
	return render_template("board.html", posts=posts, board=board)

@app.route("/boards/newboard", methods=["GET", "POST"])
def newboard():
	auth()
	if request.method == 'POST':
		board_id = request.form['board-title']
		board_summary = request.form['board-summary']
		new_board = BoardsRepo().create(board_id, board_summary)
		return redirect(url_for('boards'))
	return render_template("newboard.html")

@app.route("/friends")
def friends():
	auth()
	friends = FriendRepo().get_by_person(g.id)
	print(friends)
	return render_template("friends.html", friends=friends)


#run before each request to check session exists
@app.before_request
def before_request():
	g.user = None
	if 'user' in session:
	 	g.user = session['user']
	 	g.id = session['person_id']

#removes session upon logging out
@app.route("/logout", methods=['POST'])
def drop_session():
	session.pop('user', None)
	return redirect(url_for("login"))


#check session
def auth():
	if g.user is None:
		abort(redirect(url_for("login")))


if __name__ == "__main__":
	app.run(debug=True)


